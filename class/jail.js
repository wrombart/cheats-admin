const middleOffset = 0.2
const endOffset = 2.5
const topOffset = 7

class Jail {
    constructor(player) {
        this.bricks = []
        this.player = player
        this.bases(endOffset, endOffset)
        this.bases(endOffset, endOffset, -topOffset)
        this.bar(endOffset, endOffset)
        this.bar(-2, -2)
        this.bar(-2, endOffset)
        this.bar(endOffset, -2)
        this.bar(endOffset, middleOffset)
        this.bar(middleOffset, endOffset)
        this.bar(-2, middleOffset)
        this.bar(middleOffset, -2)
    }
    imprison() {
        if (this.player.jail) return
        this.player.jail = this
        this.player.setSpeed(0)
        this.player.setJumpPower(0)
        Game.loadBricks(this.bricks)
        this.player.spawnPosition = this.player.position
    }
    free() {
        if (!this.player.jail) return
        this.player.jail = null
        this.player.setSpeed(4)
        this.player.setJumpPower(5)
        Game.deleteBricks(this.bricks)
        this.player.spawnPosition = null
    }
    bar(x, y, z = 0) {
        this.bricks.push(new Brick(new Vector3(
            this.player.position.x - x,
            this.player.position.y - y,
            this.player.position.z - z
        ), new Vector3(0.5,0.5,7)))
    }
    bases(x, y, z = 0) {
        this.bricks.push(new Brick(new Vector3(
            this.player.position.x - x,
            this.player.position.y - y,
            this.player.position.z - z
        ), new Vector3(5, 5, 0.5)))
    }
}

module.exports = Jail