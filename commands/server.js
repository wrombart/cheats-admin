const cheatsAdmin = Game.cheatsAdmin

cheatsAdmin.createCommand({
    name: 'shutdown',
    ownerOnly: true,
    autoGetTarget: false
}, (cmd) => {
    cheatsAdmin.logAudit({
        name: cmd.name,
        caller: cmd.caller
    })
    Game.shutdown(0)
})