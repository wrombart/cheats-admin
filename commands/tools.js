const cheatsAdmin = Game.cheatsAdmin

const balloon = new Tool("balloon")
balloon.model = 84038

const wand = new Tool("wand")
wand.model = 7109

function isJailedOrFrozen(p) {
    return p.frozen || p.jail
}

cheatsAdmin.createCommand({ name: 'balloon' }, (cmd) => {
    cmd.players.forEach((p) => {
        if (p.inventory.includes(balloon)) return
        p.equipTool(balloon)
    })
})

cheatsAdmin.createCommand({ name: 'wand' }, (cmd) => {
    cmd.players.forEach((p) => {
        if (p.inventory.includes(wand)) return
        p.equipTool(wand)
    })
})

balloon.equipped((p) => !isJailedOrFrozen(p) && p.setJumpPower(12))
balloon.unequipped((p) => !isJailedOrFrozen(p) && p.setJumpPower(5))

wand.equipped((p) => p.message(`[#009ac4][Wand]: [#ffffff]This wand has magic powers when activated...`))
wand.on('activated', (_) => {
    Game.setEnvironment({
        skyColor: String(Math.random() * 99999999),
        ambient: String(Math.random() * 99999999)
    })
})