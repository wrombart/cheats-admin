const cheatsAdmin = Game.cheatsAdmin

// Boilerplate code for creating commands that use Game.setEnvironment
function createAmbientCommand(name, envProperty) {
    cheatsAdmin.createCommand({
        name: name,
        autoGetTarget: false,
    }, (cmd) => {
        Game.setEnvironment({ [envProperty]: cmd.args })
            .then(() => cheatsAdmin.logAudit({
                name: cmd.name,
                caller: cmd.caller
            }))
            .catch(() => {})
    })
}

createAmbientCommand('weather', 'weather')
createAmbientCommand('ambient', 'ambient')
createAmbientCommand('sky', 'skyColor')