const commandFiles = [
    'moderation.js',
    'fun.js',
    'environment.js',
    'server.js',
    'outfits.js',
    'tools.js'
]

module.exports = commandFiles