const cheatsAdmin = Game.cheatsAdmin

const userValue = /([^"]+)(?:\"([^\"]+)\"+)?/

const clamp = (min, max) => (value) =>
    value < min ? min : value > max ? max : value;

function adminMessage(message) {
    return `[#ff0000][V3]: [#ffffff]${message}`
}

function parseCommand(cmd) {
    const match = cmd.args.split(" ")

    const value = match.pop()
    const user = match.join(" ")

    if (!user || !value) 
        cmd.caller.message(adminMessage(`User or value was not found.`))

    return [ user, value ]
}

cheatsAdmin.createCommand({
    name: 'speed',
    autoGetTarget: false
}, (cmd) => {
    const [ user, value ] = parseCommand(cmd)
    if (!user || !value) return

    const speed = Number(value)

    const target = cheatsAdmin.getTarget(cmd, user)
    target.players.forEach((player) => {
        player.setSpeed(speed)
    })
})

cheatsAdmin.createCommand({
    name: 'jump',
    autoGetTarget: false
}, (cmd) => {
    const [ user, value ] = parseCommand(cmd)
    if (!user || !value) return

    const jumpPower = Number(value)

    const target = cheatsAdmin.getTarget(cmd, user)
    target.players.forEach((player) => {
        player.setJumpPower(jumpPower)
    })
})

cheatsAdmin.createCommand({
    name: 'score',
    autoGetTarget: false,
}, (cmd) => {
    const [ user, value ] = parseCommand(cmd)
    if (!user || !value) return

    const score = Number(value)
    if (isNaN(score)) return

    const target = cheatsAdmin.getTarget(cmd, user)
    target.players.forEach((p) => p.setScore(score))
})

cheatsAdmin.createCommand({ name: 'kill', aliases: ['slay']}, (cmd) => {
    cmd.players.forEach((p) => p.kill())
})

cheatsAdmin.createCommand({ name: 'god' }, (cmd) => {
    cmd.players.forEach((p) => {
        p.setHealth(Number.MAX_SAFE_INTEGER)
        p.message(adminMessage("You now have the health of the gods."))
    })
})

cheatsAdmin.createCommand({ name: 'respawn' }, (cmd) => {
    cmd.players.forEach((p) => p.respawn())
})

cheatsAdmin.createCommand({
    name: 'size',
    aliases: ['scale'],
    autoGetTarget: false
}, (cmd) => {
    const [ user, value ] = parseCommand(cmd)
    if (!user || !value) return

    const scaleCoords = value.split(",")

    if (scaleCoords.length === 2 || scaleCoords.length > 3) return

    for (let index in scaleCoords) {
        const coord = Number(scaleCoords[index])
        if (isNaN(coord)) return
        scaleCoords[index] = clamp(cheatsAdmin.minScale, cheatsAdmin.maxScale)(coord)
    }

    const parsedScale = (scaleCoords.length === 1) ? 
        new Vector3(scaleCoords[0], scaleCoords[0], scaleCoords[0]) : 
        new Vector3(...scaleCoords)

    const target = cheatsAdmin.getTarget(cmd, user)
    target.players.forEach((victim) => victim.setScale(parsedScale))
})

cheatsAdmin.createCommand({
    name: 'tp',
    audit: false,
    autoGetTarget: false
}, (cmd) => {
    let [ user, value ] = parseCommand(cmd)
    if (!user || !value) return
    
    value = value.split(",")

    let position, target;

    if (value.length === 1) {
        target = cheatsAdmin.getTarget(cmd, value[0])
        if (target.players.length) {
            target = target.players.shift()
            position = new Vector3().fromVector(target.position)
        }
    } else if (value.length === 3) {
        position = new Vector3(Number(value[0]), Number(value[1]), Number(value[2]))   
    }

    if (!position) return

    cheatsAdmin.logAudit({ 
        name: cmd.name, 
        caller: cmd.caller, 
        target: (target && target.username)
    })

    const teleportTargets = cheatsAdmin.getTarget(cmd, user)
    teleportTargets.players.forEach((p) => p.setPosition(position))
})

cheatsAdmin.createCommand({
    name: 'team',
    autoGetTarget: false
}, (cmd) => {
    const match = cmd.args.match(userValue)
    if (!match[1] || !match[2]) return

    const user = match[1] && match[1].trim()
    const teamName = match[2]

    let team = world.teams.find(t => t.name.toLowerCase() === teamName.toLowerCase())
    if (!team) {
        team = new Team(teamName)
        Game.newTeam(team)
    }
    
    const target = cheatsAdmin.getTarget(cmd, user)
    target.players.forEach((p) => p.setTeam(team))
})