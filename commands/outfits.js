const cheatsAdmin = Game.cheatsAdmin

// Yes, this is ugly.
const outfits = {
    frozen:     new Outfit()
                    .body("#0091ff"),

    cyclops:    new Outfit()
                    .face(34345)
                    .body("#0091ff")
                    .hat1(1)
                    .hat2(1)
                    .hat3(1),

    weed:       new Outfit()
                    .face(420)
                    .body("#00ff00")
                    .hat1(42599)
                    .hat2(1)
                    .hat3(1),

    snowman:    new Outfit()
                    .face(6361)
                    .body("#ffffff")
                    .hat1(88877)
                    .hat2(27948)
                    .hat3(1),

    zombie:     new Outfit()
                    .face(76559)
                    .head("#0d9436")
                    .torso("#694813")
                    .leftArm("#0d9436")
                    .rightArm("#0d9436")
                    .leftLeg("#694813")
                    .rightLeg("#694813")
                    .hat1(1)
                    .hat2(1)
                    .hat3(1),

    basil:      new Outfit()
                    .face(94126)
                    .body("#000000")
                    .hat1(1937)
                    .hat2(1)
                    .hat3(1),

    creeper:    new Outfit()
                    .face(33349)
                    .body("#00ff00")
                    .hat1(1)
                    .hat2(1)
                    .hat3(1)
}

function createOutfitCommand(name) {
    const outfitData = outfits[name]
    cheatsAdmin.createCommand({ name: name }, (cmd) => {
        cmd.players.forEach((player) => player.setOutfit(outfitData))
    })
}

Object.keys(outfits).forEach((outfit) => createOutfitCommand(outfit))